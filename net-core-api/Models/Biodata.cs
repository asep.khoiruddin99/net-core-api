﻿using System;

namespace net_core_api.Models
{
	public partial class Biodata
	{
        public int BiodataID { get; set; }
        public string NamaLengkap { get; set; } = null!;
        public string TempatLahir{ get; set; } = null!;
        public string TanggalLahir{ get; set; } = null!;
        public string JenisKelamin{ get; set; } = null!;
        public string Alamat{ get; set; } = null!;
        public string NamaAyah{ get; set; } = null!;
        public string NamaIbu{ get; set; } = null!;
        public string NoTelepon { get; set; } = null!;
    }


    public class BiodataRequest
    {
        public string NamaLengkap { get; set; } = null!;
        public string TempatLahir { get; set; } = null!;
        public string TanggalLahir { get; set; } = null!;
        public string JenisKelamin { get; set; } = null!;
        public string Alamat { get; set; } = null!;
        public string NamaAyah { get; set; } = null!;
        public string NamaIbu { get; set; } = null!;
        public string NoTelepon { get; set; } = null!;
    }
}


﻿using net_core_api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace net_core_api.Data
{
    public partial class SiswaDbContext : DbContext
    {
        public SiswaDbContext()
        { }

        public SiswaDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Biodata> Biodata { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=sql_server_edge;Database=Siswa;Trusted_Connection=True;User Id=sa;Password=padepokan79;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Biodata>(entity =>
            {
                entity.HasKey(e => e.BiodataID);

                entity.ToTable("Biodata");

                entity.Property(e => e.NamaLengkap)
                    .HasMaxLength(225)
                    .IsUnicode(false)
                    .HasColumnName("NamaLengkap");

                entity.Property(e => e.TempatLahir)
                    .HasMaxLength(225)
                    .IsUnicode(false)
                    .HasColumnName("TempatLahir");

                entity.Property(e => e.TanggalLahir)
                    .HasMaxLength(225)
                    .IsUnicode(false)
                    .HasColumnName("TanggalLahir");

                entity.Property(e => e.JenisKelamin)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("JenisKelamin");

                entity.Property(e => e.TanggalLahir)
                    .HasMaxLength(225)
                    .IsUnicode(false)
                    .HasColumnName("TanggalLahir");

                entity.Property(e => e.Alamat)
                    .HasMaxLength(225)
                    .IsUnicode(false)
                    .HasColumnName("Alamat");

                entity.Property(e => e.NamaAyah)
                    .HasMaxLength(225)
                    .IsUnicode(false)
                    .HasColumnName("NamaAyah");

                entity.Property(e => e.NamaIbu)
                    .HasMaxLength(225)
                    .IsUnicode(false)
                    .HasColumnName("NamaIbu");

                entity.Property(e => e.NoTelepon)
                    .HasMaxLength(225)
                    .IsUnicode(false)
                    .HasColumnName("NoTelepon");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}


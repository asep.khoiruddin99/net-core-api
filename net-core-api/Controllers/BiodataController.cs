﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using net_core_api.Data;
using net_core_api.Models;
using StackExchange.Redis;

namespace net_core_api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class BiodataController : Controller
	{
		private readonly SiswaDbContext dbContext;
        private readonly IConnectionMultiplexer dbRedis;

        public BiodataController(SiswaDbContext Context, IConnectionMultiplexer Redis)
		{
			//this.dbContext = dbContext;
			dbContext = Context;
			dbRedis = Redis;
		}

		[HttpGet]
		public async Task<ActionResult<IEnumerable<Biodata>>> GetBiodata()
		{
            //return Ok(dbContext.Biodata.ToList());
            if (dbContext.Biodata == null)
            {
                return NotFound();
            }
            return await dbContext.Biodata.ToListAsync();
        }

		[HttpPost]
		public async Task<IActionResult> PostBiodata(BiodataRequest biodataRequest)
		{
			var biodata = new Biodata()
			{
				NamaLengkap = biodataRequest.NamaLengkap,
				TempatLahir = biodataRequest.TempatLahir,
				TanggalLahir = biodataRequest.TanggalLahir,
				JenisKelamin = biodataRequest.JenisKelamin,
				Alamat = biodataRequest.Alamat,
				NamaAyah = biodataRequest.NamaAyah,
				NamaIbu = biodataRequest.NamaIbu,
				NoTelepon = biodataRequest.NoTelepon
			};

			await dbContext.Biodata.AddAsync(biodata);
			await dbContext.SaveChangesAsync();

			return Ok(biodata);
		}

        [HttpGet("{id}")]
        public async Task<ActionResult<Biodata>> GetBiodata(string id)
        {
            var db = dbRedis.GetDatabase();
            if (db.HashExists($"biodata:{id}", "BiodataID"))
            {
                var key = $"biodata:{id}";
                var b = await db.HashGetAllAsync(key);
                var btemp = b.ToList();
                btemp.Add(new HashEntry("cache", true));
                return Ok(string.Join("\n", btemp.Select(b => $"{b.Name}: {b.Value}")));
            }

            if (dbContext.Biodata == null)
            {
                return NotFound();
            }
            var biodata = await dbContext.Biodata.FindAsync(id);

            if (biodata == null)
            {
                return NotFound();
            }

            await SetRedis(biodata);

            return biodata;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task SetRedis(Biodata biodata)
        {
            var db = dbRedis.GetDatabase();
            await db.HashSetAsync($"biodata:{biodata.BiodataID}", new HashEntry[]
            {
                new HashEntry(nameof(biodata.NamaLengkap), biodata.NamaLengkap),
                new HashEntry(nameof(biodata.TempatLahir), biodata.TempatLahir),
                new HashEntry(nameof(biodata.TanggalLahir), biodata.TanggalLahir),
                new HashEntry(nameof(biodata.JenisKelamin), biodata.JenisKelamin),
                new HashEntry(nameof(biodata.Alamat), biodata.Alamat),
                new HashEntry(nameof(biodata.NamaAyah), biodata.NamaAyah),
                new HashEntry(nameof(biodata.NamaIbu), biodata.NamaIbu),
                new HashEntry(nameof(biodata.NoTelepon), biodata.NoTelepon),
            });
        }
    }
}

